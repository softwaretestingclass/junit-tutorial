# JUnit Tutorial

## Examples Based on

The Art of Unit Testing
Second Edition
Roy Osherove

## What is a good unit test?
- Automatic and repeatable
- Should be relevant tomorrow
- Should run at the push of a button
- Should run quickly
- Consistent in its results
- Should be fully isolated

## How's it different from an integration test?
- Integration is slower
- Testing integration with external dependencies
    - Database
    - Filesystem
    - Other service

## Conventions for test method names 
```
#!

[UnitOfWorkName]_[ScenarioUnderTest]_[ExpectedBehavior]
```

    
## Tests have three parts
- Arrange
    - Setup unit under test
- Act
    - Perform action on unit under test
- Assert
    - Verify behavior of unit with respect to expected behavior