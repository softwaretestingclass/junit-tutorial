package cen4270;

import junitparams.JUnitParamsRunner;
import junitparams.Parameters;
import org.junit.*;
import org.junit.runner.RunWith;

import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.*;

@RunWith(JUnitParamsRunner.class)
public class LogAnalyzerTest {
    private LogAnalyzer analyzer;
    private ExtensionManager stubExtensionManager;

    @Before
    public void setup() {
        stubExtensionManager = mock(ExtensionManager.class);
        this.analyzer = new LogAnalyzer(stubExtensionManager);
    }

    @Test
    public void isValidFilename_EmptyFileName_ThrowsException() {
        Exception expectedException = null;

        try {
            analyzer.isValidLogFileName("");
        } catch (IllegalArgumentException e) {
            expectedException = e;
        }

        Assert.assertNotNull(expectedException);
        Assert.assertEquals(expectedException.getMessage(), "File name has to be provided");
    }

    @Test
    public void isValidFilename_BadExtension_ReturnsFalse() {
        when(stubExtensionManager.isValid(anyString())).thenReturn(false);

        boolean result = analyzer.isValidLogFileName("filewithbadextension.foo");

        Assert.assertFalse(result);
    }

    @Test
    @Parameters({
            "filewithgoodextension.slf",
            "filewithgoodextension.SLF"
    })
    public void isValidFilename_ValidExtensions_ReturnsTrue(String fileName) {
        when(stubExtensionManager.isValid(anyString())).thenReturn(true);

        boolean result = analyzer.isValidLogFileName(fileName);

        Assert.assertTrue(result);
    }

    @Test
    public void analyze_TooShortFilename_CallsWebService() throws Exception {
        WebServiceClient mockService = mock(WebServiceClient.class);
        analyzer.setWebServiceClient(mockService);

        String tooShortFilename = "abc.slf";
        analyzer.analyze(tooShortFilename);

        verify(mockService).logError("File name is too short: " + tooShortFilename);
    }

    @Test
    public void analyze_WebServiceThrows_SendEmail() throws Exception {
        Exception exceptionToThrow = new Exception("Fake exception");
        WebServiceClient stubService = mock(WebServiceClient.class);
        doThrow(exceptionToThrow).when(stubService).logError(anyString());

        EmailService mockEmailService = mock(EmailService.class);

        analyzer.setWebServiceClient(stubService);
        analyzer.setEmailService(mockEmailService);

        String tooShortFilename = "abc.slf";
        analyzer.analyze(tooShortFilename);

        EmailInfo expectedEmail = new EmailInfo(
            "admin@logs.com",
            "Can't log",
            exceptionToThrow.getMessage()
        );

        verify(mockEmailService).sendEmail(expectedEmail);
    }
}
