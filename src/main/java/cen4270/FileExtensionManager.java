package cen4270;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.HashSet;
import java.util.Scanner;
import java.util.Set;

public class FileExtensionManager implements ExtensionManager {
    public boolean isValid(String fileName) {
        Set<String> validExtensions = new HashSet<String>();

        try {
            ClassLoader classLoader = getClass().getClassLoader();
            File file = new File(classLoader.getResource("valid_extensions.conf").getFile());

            Scanner scan = new Scanner(file);
            while(scan.hasNext()) {
                String validExtension = scan.next().toLowerCase();
                validExtensions.add(validExtension);
            }
        } catch (FileNotFoundException e) {
            System.err.println("Valid Extensions file not found!");
            return false;
        }

        fileName = fileName.toLowerCase();
        for(String validExtension : validExtensions) {
            if(fileName.endsWith(validExtension)) {
                return true;
            }
        }

        return false;
    }
}
