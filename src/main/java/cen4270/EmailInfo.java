package cen4270;

public class EmailInfo {
    public String to;
    public String title;
    public String body;

    public EmailInfo(String to, String title, String body) {
        this.to = to;
        this.title = title;
        this.body = body;
    }

    public String getTo() {
        return to;
    }

    public void setTo(String to) {
        this.to = to;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getBody() {
        return body;
    }

    public void setBody(String body) {
        this.body = body;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        EmailInfo emailInfo = (EmailInfo) o;

        if (to != null ? !to.equals(emailInfo.to) : emailInfo.to != null) return false;
        if (title != null ? !title.equals(emailInfo.title) : emailInfo.title != null) return false;
        return body != null ? body.equals(emailInfo.body) : emailInfo.body == null;

    }

    @Override
    public int hashCode() {
        int result = to != null ? to.hashCode() : 0;
        result = 31 * result + (title != null ? title.hashCode() : 0);
        result = 31 * result + (body != null ? body.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "EmailInfo{" +
                "to='" + to + '\'' +
                ", title='" + title + '\'' +
                ", body='" + body + '\'' +
                '}';
    }
}
