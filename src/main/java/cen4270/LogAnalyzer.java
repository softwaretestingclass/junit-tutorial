package cen4270;

public class LogAnalyzer {
    private ExtensionManager extensionManager;
    private WebServiceClient webServiceClient;
    private EmailService emailService;

    public LogAnalyzer(ExtensionManager extensionManager) {
        this.extensionManager = extensionManager;
    }

    public void setWebServiceClient(WebServiceClient webServiceClient) {
        this.webServiceClient = webServiceClient;
    }

    public void setEmailService(EmailService emailService) {
        this.emailService = emailService;
    }

    public boolean isValidLogFileName(String fileName) {
        if(fileName == null || fileName.isEmpty()) {
            throw new IllegalArgumentException("File name has to be provided");
        }

        return extensionManager.isValid(fileName);
    }

    public void analyze(String fileName) {
        if(fileName.length() < 8) {
            try {
                webServiceClient.logError("File name is too short: " + fileName);
            } catch (Exception e) {
                emailService.sendEmail(
                    new EmailInfo(
                        "admin@logs.com",
                        "Can't log",
                        e.getMessage()
                    )
                );
            }
        }
    }
}
